$(document).ready(function(){
	$("#balance").click(function(){
		$.ajax({
			type:'post',
			url:'ajax.php',
			data:{type:'user_info',action:'authenticated_api'},
			success:function(r){
				r = JSON.parse(r);
				
				$("#user_info").empty()
				$("#user_info").append('<p><b>user id -</b> '+r['uid']+'</p>');
				$("#user_info").append('<p><b>Balance</b></p>\
										<table>\
											<tr>\
												<th>currency</th>\
												<th>balance</th>\
											</tr>\
											<tr>\
												<td>USD</td>\
												<td>'+r['balances']['USD']+'</td>\
											</tr>\
											<tr>\
												<td>EUR</td>\
												<td>'+r['balances']['EUR']+'</td>\
											</tr>\
											<tr>\
												<td>RUB</td>\
												<td>'+r['balances']['RUB']+'</td>\
											</tr>\
										</table>');
			}
		})
	})
});